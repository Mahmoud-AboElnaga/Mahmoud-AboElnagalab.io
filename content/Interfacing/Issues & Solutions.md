---
title: Issues & Solutions
weight: -20
---

## Here I will provide the issues I faced and how I solved them:

**Issue:** When I opened the **interface.pde** on processing, there was an error named "Syntax error on token "interface", Identifier expected"

[![i6](/media/i6.png)](/media/i6.png)

**Solution:** I searched for this error and found that this error is relating to "interface" name, it is a reserved word in Java, so I renamed the files to "Hello" to be a genaeral name

[![i13](/media/i13.png)](/media/i13.png)

**Issue:** After completing the missing lines in the **interface** code, run the pattern, it was not similar to the actual leds on the PCB.

**Solution:** I edited the following lines to make the interface similar to the board to have the same pattern

[![i14](/media/i14.png)](/media/i14.png)
[![i15](/media/i15.png)](/media/i15.png)
[![i16](/media/i16.png)](/media/i16.png)
[![i17](/media/i17.png)](/media/i17.png)

