---
title: 2- Editing Processing Code
weight: -20
---
● Download and install [Processing](https://processing.org/download/)

[![i5](/media/i5.png)](/media/i5.png)

● Open the **interface.pde** file

[![i6](/media/i6.png)](/media/i6.png)

● Change the serial port to [1] instead of [0]

[![i18](/media/i18.png)](/media/i18.png)

● Implement the if else condition for the 3rd button

[![i7](/media/i7.png)](/media/i7.png)

● Implement the if else condition for the 6th button

[![i8](/media/i8.png)](/media/i8.png)

● Implement if condition for the click on the 5th and 6th leds

[![i9](/media/i9.png)](/media/i9.png)

● From **File** select **Save**








