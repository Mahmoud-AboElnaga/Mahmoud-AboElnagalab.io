---
title: 3- Running Interface
weight: -20
---
● Run the processing code by pressing the shown button.

[![i10](/media/i10.png)](/media/i10.png)

● The interfacing window appears as shown 

*we have 6 rectangles/leds and a press button*

[![i11](/media/i11.png)](/media/i11.png)

● When we select a pattern and press the bottom rectangle, the pattern shows on the board with hte same pattern

[![i12](/media/i12.png)](/media/i12.png)

● [Watch it now](https://www.youtube.com/watch?v=73xszp3eCZg)










