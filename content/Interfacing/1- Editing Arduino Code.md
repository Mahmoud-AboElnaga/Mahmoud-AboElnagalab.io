---
title: 1- Editing Arduino Code
weight: -20
---
● Open **demo.ino** with **Arduino IDE**, comment and uncomment some lines as shown:

[![i1](/media/i1.png)](/media/i1.png)

● Connect the Progammer and the FTDI cable between the board and laptop.

[![i4](/media/i4.png)](/media/i4.png)

● From **Tools**, Make sure the following items are selected:

	- Board > ATtiny25/45/85
	- Processor > ATtiny45
	- Clock > Internal 8 MHz
	- Programmer: Atmel-ICE (AVR)
	
[![i2](/media/i2.png)](/media/i2.png)
	
● From **Sketch**, select **Upload Using Programmer**

[![i3](/media/i3.png)](/media/i3.png)

● Now! the new code is uploaded and the leds are not flasing.
