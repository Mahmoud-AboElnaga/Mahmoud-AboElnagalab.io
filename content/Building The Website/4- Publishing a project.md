---
title: 4- Publishing a project
weight: -20
---

● In the gitlab account, go to your project's Settings > General > Advanced and
Change Path to Mahmoud-AboElnaga.gitlab.io

[![20](/media/20.png)](/media/20.png)

● In the local project folder, open config.yaml in notepad and Change baseURL: https://ahmad._.saeed.gitlab.io/ to https://Mahmoud-AboElnaga.gitlab.io/

[![21](/media/21.png)](/media/21.png)

● Open Sourcetree and stage all the changes

[![22](/media/22.png)](/media/22.png)

● Click on **Commit** with a message

[![23](/media/23.png)](/media/23.png)

● As it is the first time, It is required to ender the user name and password

[![24](/media/24.png)](/media/24.png)

● Push all the changes to the online website

[![25](/media/25.png)](/media/25.png)

● It is now pushed online through GitLab

[![26](/media/26.png)](/media/26.png)

● Open [my website](https://mahmoud-aboelnaga.gitlab.io/)

[![27](/media/27.png)](/media/27.png)

