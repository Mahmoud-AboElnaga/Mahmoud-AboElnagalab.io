---
title: 1- Git & GitLab Overview
weight: -20
---
## Git

Git is a version control system used to track changes in computer files. Git's primary purpose is to manage any changes made in one or more projects over a given period of time. It helps coordinate work among members of a project team and tracks progress over time. Git also helps both programming professionals and non-technical users by monitoring their project files. 

Git can handle projects of any size. It allows multiple users to work together without having affecting each other's work

[![GitAreas](/media/GitAreas.png)](/media/GitAreas.png)

The working tree is a single checkout of one version of the project. These files are pulled out of the compressed database in the Git directory and placed on disk for you to use or modify.

The staging area is a file, generally contained in your Git directory, that stores information about what will go into your next commit. Its technical name in Git parlance is the “index”, but the phrase “staging area” works just as well.

The Git directory is where Git stores the metadata and object database for your project. This is the most important part of Git, and it is what is copied when you clone a repository from another computer.

The basic Git workflow goes something like this:

**First**, you modify files in your working tree.

**Second**, you selectively stage just those changes you want to be part of your next commit, which adds only those changes to the staging area.

**Third**, you do a commit, which takes the files as they are in the staging area and stores that snapshot permanently to your Git directory.

*To know more about Git click [here](https://git-scm.com/)*

## GitLab

GitLab is a web-based Git repository that provides free open and private repositories, issue-following capabilities, and wikis. It is a complete DevOps platform that enables professionals to perform all the tasks in a project—from project planning and source code management to monitoring and security. Furthermore, it allows teams to collaborate and build better software. 

GitLab helps teams reduce product lifecycles and increase productivity, which in turn creates value for customers. The application doesn't require users to manage authorizations for each tool. If permissions are set once, then everyone in the organization has access to every component.
GitLab started as an open source project to help teams collaborate on software development. GitLab's mission is to provide a place where everyone can contribute. Each team member uses our product internally and directly impacts the company roadmap. This exceptional approach works because we're a team of passionate people who want to see each other, the company, and the broader GitLab community succeed and we have the platform to make that possible.


*To know more about GitLab click [here](https://about.gitlab.com/)*

