---
title: 3- Cloning a project to PC
weight: -20
---

● Download [Source tree](https://www.sourcetreeapp.com/)

[![10](/media/10.png)](/media/10.png)

● Configure settings with User name and email 

[![11](/media/11.png)](/media/11.png)

● From SourceTree > Tools > Create SSH Keys > Generate

[![13](/media/13.png)](/media/13.png)

● Save both public and private keys to an **.shh** folder

[![14](/media/14.png)](/media/14.png)

● Gitlab > Settings > SSH Keys, paste the SSH key generated fron the previous step.

[![15](/media/15.png)](/media/15.png)

● Now SSH key is working well

[![16](/media/16.png)](/media/16.png)

● Clone the project locally

[![18](/media/18.png)](/media/18.png)

