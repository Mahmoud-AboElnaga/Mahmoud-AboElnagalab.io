---
title: Issues & Solutions
weight: -20
---

*Here I will provide the issues I faced and how I solved them:*

**Issue**: Images do not appear in the website

**Solution**: I checked the extension of the images, it was .png not .jpg, so I changed them and they appeared well in the website


**Issue**: My photo is too large in the web page 

**Solution**: I used columns to divide the web page area, so I used the following lines 

[![32](/media/32.png)](/media/32.png)

**Issue**: When I pushed some changes, a failed pipe line appears in Gitlab

[![100](/media/100.png)](/media/100.png)

**Solution**: I checked the last changes I made, resolved them and pushed again 