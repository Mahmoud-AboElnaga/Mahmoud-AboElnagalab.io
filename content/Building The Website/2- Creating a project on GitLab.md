---
title: 2- Creating a project on GitLab
weight: -20
---

● Go to [GitLab.com](https://gitlab.com/)

[![1](/media/1.png)](/media/1.png)

● Create a new account 

[![2](/media/2.png)](/media/2.png)

● Now! I have a gitLab account and can edit my profile

[![3](/media/3.png)](/media/3.png)

● *It is important to* confirm the the account through the email sent

[![4](/media/4.png)](/media/4.png)

● I tried to upload and set a photo of myself

[![6](/media/6.png)](/media/6.png)

● Open this [project](https://gitlab.com/ahmad._.saeed/ahmad._.saeed.gitlab.io) and click on **Fork** in the top reight.

[![7](/media/7.png)](/media/7.png)

● Now! the project is successfully forked to my repo

[![8](/media/8.png)](/media/8.png)

● Remove the Fork relationship between my project and the previous.

[![9](/media/9.png)](/media/9.png)
