---
title: 5- Editing the Content
weight: -20
---

● In order to edit my website content, I need a text editor to do that, so I downloaded and installed [Notepad++](https://notepad-plus-plus.org/downloads/)

[![28](/media/28.png)](/media/28.png)

● In Notepad++ I have to open the file I want to edit, for example getting started.md

[![29](/media/29.png)](/media/29.png)

● I searched for [**Markdown Basic Syntax**](https://www.markdownguide.org/basic-syntax/) to be able to edit and add styles to the text, like **Bold**, *Italic* and more

- To bold text, add two asterisks or underscores before and after a word or phrase. 
- To italicize text, add one asterisk or underscore before and after a word or phrase.
- To emphasize text with bold and italics at the same time, add three asterisks or underscores before and after a word or phrase.

● To upload a photo to a web page, I have to write down the following line

  > [![33](/media/33.png)](/media/33.png)
  
● Each change I made in a file in Notepad++, I have to go to **Sourcetree** and commit all the stages 
  *it is important to write down a message for each commit to remember the change I made*
  
  [![23](/media/23.png)](/media/23.png)
  
● Then push all the changes to upload them to the online website 

[![25](/media/25.png)](/media/25.png)
