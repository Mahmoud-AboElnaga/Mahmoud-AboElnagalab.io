---
title: Mahmoud Abo Elnaga
weight: -20
---
{{< columns >}} 
is a young and passionate maker, 27 years old,
received a BSc degree in Production Engineering and Mechanical Design, completed six engineering internships in various industries, loves Making, DIYing and Digital Fabrication, completed the American Center Cairo and Fab Lab Egypt’s program “The Advanced Maker Diploma”, participated in organizing Maker Fair Cairo 2018 , 2019 , 2020 and Fab 15 Conference and now, work at Fab Lab Egypt, as a Technical Operation Manager.


## Why Fab Academy?

I would like to join Fab Academy for two reasons:
**First** I love Making, DIYing and Digital Fabrication, I Completed the American Center Cairo and Fab Lab Egypt’s program “The Advanced Maker Diploma” and fabricated a modular CNC machine. So, I would like to enhance my skills and experience in digital fabrication and to master the designing and manufacturing of CNC and 3d printing machines.
**Second** I have joined Fab Lab Egypt as a Fab Lab Specialist since June 2018 and get promoted to be a Fab Lab Supervisor. So, Joining the Fab Academy program will enhance my technical and managerial skills and will assist me to get higher positions in the digital fabrication career.

<--->
[![My Photo](/media/Mahmoud.jpg)](/media/Mahmoud.jpg)
