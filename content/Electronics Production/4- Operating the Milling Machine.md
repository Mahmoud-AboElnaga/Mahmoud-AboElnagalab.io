---
title: 4- Operating the Milling Machine
weight: -20
---
## I will be using the 3040 CNC Milling Machine which supports .gcode files and uses OpenBuilds controller

{{< columns >}}
[![147](/media/147.jpg)](/media/147.jpg)
<--->
{{< /columns >}}

1. Prepare a single layer PCB board with paint tape and double face.

{{< columns >}}
[![148](/media/148.jpg)](/media/148.jpg)
<--->
[![149](/media/149.jpg)](/media/149.jpg)
<--->
[![150](/media/150.jpg)](/media/150.jpg)
{{< /columns >}}

2. Fix the **0.4 v-bit** in the spindle 

{{< columns >}}
[![151](/media/151.jpg)](/media/151.jpg)
<--->
{{< /columns >}}

3. Fix the PCB board on the sacrificial layer of the bed. 

{{< columns >}}
[![152](/media/152.jpg)](/media/152.jpg)
<--->
{{< /columns >}}

4. Open the controller 

{{< columns >}}
[![153](/media/153.jpg)](/media/153.jpg)
<--->
{{< /columns >}}

5. Move the X, Y and Z axes to the front left corner of the board

{{< columns >}}
[![154](/media/154.jpg)](/media/154.jpg)
<--->
{{< /columns >}}

6. Open the spindle with high speed

{{< columns >}}
[![155](/media/155.jpg)](/media/155.jpg)
<--->
{{< /columns >}}

7. Open OpenBuilds Control and connect the machine as **FTDI** Port

{{< columns >}}
[![162](/media/162.png)](/media/162.png)
<--->
{{< /columns >}}

8. Now the machine is connected and ready 

{{< columns >}}
[![163](/media/163.png)](/media/163.png)
<--->
{{< /columns >}}

9. From the **Open G-CODE** menu, select the **Open G-CODE from file** and select the **Top** file

{{< columns >}}
[![164](/media/164.png)](/media/164.png)
<--->
{{< /columns >}}

10. Clock on **RUN**, tha machine will run the top file which appears as follows

{{< columns >}}
[![156](/media/156.jpg)](/media/156.jpg)
<--->
{{< /columns >}}

11. Use the **0.8 mm drilling bit** to drill the holes

{{< columns >}}
[![157](/media/157.jpg)](/media/157.jpg)
<--->
{{< /columns >}}

12. Open the **Holes.gcode** file and **RUN** the machine

{{< columns >}}
[![165](/media/165.png)](/media/165.png)
<--->
{{< /columns >}}

13. Use the **1.5 mm end mill** to perform the outline cutting

{{< columns >}}
[![158](/media/158.jpg)](/media/158.jpg)
<--->
{{< /columns >}}

14. Open the **Outline.gcode** file and **RUN** The machine

{{< columns >}}
[![168](/media/168.png)](/media/168.png)
<--->
{{< /columns >}}

15. The machine will run the outline as shown

{{< columns >}}
[![159](/media/159.jpg)](/media/159.jpg)
<--->
{{< /columns >}}

16. Now the PCB is machines and ready for **soldering**

{{< columns >}}
[![160](/media/160.jpg)](/media/160.jpg)
<--->
[![161](/media/161.jpg)](/media/161.jpg)
{{< /columns >}}