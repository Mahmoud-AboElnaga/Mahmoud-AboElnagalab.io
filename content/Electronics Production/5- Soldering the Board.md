---
title: 5- Soldering the Board
weight: -20
---
## Required Tools for Soldering:

  ● Soldering Iron
  
  ● Solder
  
  ● Gift
  
  ● Desoldering Pump
  
 [![169](/media/169.jpg)](/media/169.jpg)
 
 ## Required Components to be soldered:
 
  ● [6] Green LEDs
  
  ● [1] ATTINY 45
  
  ● [1] ISP Header 2x3
  
  ● [1] FTDI Header & FTDI programmer
  
  ● [1] Capacitor 1uf (1206)
  
  ● [1] Resisror 10K 
  
  ● [3] Resistors 499 ohm  
  

## Soldering Tips and Tricks: 

● Make sure that the orientation of the components is the same as in the design.

● For LEDs, match the cathode in the design to the green strip on the physical component.

● For the Microcontroller, there is a notch that marks pin number 1.

● Resistors and ceramic capacitors are indifferent to their orientation.

● For easy soldering, solder the PCB starting with from the centre to out and with smaller components then larger ones.

{{< columns >}}
[![s1](/media/s1.png)](/media/s1.png)
<--->
[![s2](/media/s2.png)](/media/s2.png)
{{< /columns >}}

{{< columns >}}
[![s3](/media/s3.png)](/media/s3.png)
<--->
[![s4](/media/s4.png)](/media/s4.png)
{{< /columns >}}
