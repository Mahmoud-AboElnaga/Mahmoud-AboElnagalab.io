---
title: 3- Preparing G Code Files
weight: -20
---
**Now! We have three .png files and want to generate the **G code** for each image, I will be using [Fab Modules](http://fabmodules.org/) in this stage**

● Open [Fab Modules](http://fabmodules.org/) into the browser.

[![132](/media/132.png)](/media/132.png)

● Select **image (.png)** from **input format** and select the **Top** file

[![133](/media/133.png)](/media/133.png)

● Select **G-codes(.nc)** from **output format**

[![134](/media/134.png)](/media/134.png)

● Select **PCB traces (1/64)** from **process**

[![135](/media/135.png)](/media/135.png)

● In the right colomn make sure the **dpi is 700**, set the plunge speed to 2 mm/s and jog height to 2 mm and make sure the cut speed is 4 mm/s and use the remaining default settings.

[![136](/media/136.png)](/media/136.png)
[![137](/media/137.png)](/media/137.png)

● Press **Claculate** then **Save** 

[![138](/media/138.png)](/media/138.png)

● Select **image (.png)** from **input format** and select the **Holes** file
● Select **G-codes(.nc)** from **output format** 
● Select **PCB outline (1/32)** from **process**

[![139](/media/139.png)](/media/139.png)

● In the right colomn make sure the **dpi is 700**, set the plunge speed to 2 mm/s and jog height to 2 mm and make sure the cut speed is 4 mm/s and use the remaining default settings.
● Press **Claculate** then **Save** 

[![140](/media/140.png)](/media/140.png)
[![141](/media/141.png)](/media/141.png)
[![142](/media/142.png)](/media/142.png)

● Select **image (.png)** from **input format** and select the **Outline** file
● Select **G-codes(.nc)** from **output format** 
● Select **PCB outline (1/32)** from **process**

[![143](/media/143.png)](/media/143.png)

● In the right colomn make sure the **dpi is 700**, set the plunge speed to 2 mm/s and jog height to 2 mm and make sure the cut speed is 4 mm/s and use the remaining default settings.
● Press **Claculate** then **Save** 

{{< columns >}}
[![144](/media/144.png)](/media/144.png)
<--->
[![145](/media/145.png)](/media/145.png)
<--->
[![146](/media/146.png)](/media/146.png)
{{< /columns >}}






