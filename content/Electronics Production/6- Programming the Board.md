---
title: 6- Programming the Board
weight: -20
---
## Required tools for programming:

● ATMEL ICE programmer  

● FTDI to USB converter

● PC/Laptop with Arduino IDE installed

● The PCB board 

## Programming Steps:

1. Connect the Atmel ICE programmer to the board and the laptop

2. Connect the FTDI converter to the board and the laptop

3. Open the **demo.ino** file using Arduino.

4. Add a link to support the ATtiny45 package by going to file->preferences->Additional Boards Manager URLs then paste
this [link](https://raw.githubusercontent.com/damellis/attiny/ide-1.6.x-boards-manager/package_damellis_attiny_index.json)

{{< columns >}}
[![172](/media/172.png)](/media/172.png)
<--->
[![173](/media/173.png)](/media/173.png)
<--->
{{< /columns >}}

5. Install the support package by going to tools -> Boards -> Boards Manager
then search for attiny and click install.

{{< columns >}}
[![170](/media/170.png)](/media/170.png)
<--->
[![171](/media/171.png)](/media/171.png)
<--->
{{< /columns >}}

6. Go to tools -> Boards -> ATtiny microcontrollers -> then select Attiny25/45/85

[![174](/media/174.png)](/media/174.png)

7. Go to tools -> processor -> then select ATtiny45

[![175](/media/175.png)](/media/175.png)

8. Go to tools -> clock -> then select internal 8 MHz.

[![176](/media/176.png)](/media/176.png)

9. Configuring the programmer by going to tools -> Programmers -> then select Atmel ICE (AVR).

[![177](/media/177.png)](/media/177.png)

10. Select Sketch > click upload to compile and upload the code to the microcontroller.

[![178](/media/178.png)](/media/178.png)

11. Now! it is flashing and working well. **[Watch it Now](https://www.youtube.com/watch?v=4_KWY8DSt-k)**




