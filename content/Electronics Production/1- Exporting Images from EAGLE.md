---
title: 1- Exporting Images from EAGLE
weight: -20
---
● Download and Install [AUTODESK Eagle](https://www.autodesk.com/products/eagle/free-download?plc=F360&term=1-YEAR&support=ADVANCED&quantity=1)

[![101](/media/101.png)](/media/101.png)

● Open EAGLE, Select **Open** from the **File** menu, select **Schematic** and then select the **LED_matrix.sch**

[![102](/media/102.png)](/media/102.png)
[![103](/media/103.png)](/media/103.png)

● Shown is the **Schematic/Circuit diagram** of the LED_matrix Board

[![104](/media/104.png)](/media/104.png)

● You can switch between the **Schematic** and **Board Layout** through the shown button.

[![105](/media/105.png)](/media/105.png)

● Shown is the **Board Layout** of the LED_matrix Board

[![106](/media/106.png)](/media/106.png)

● It is important to show only the Top, Pads and Vias layers as follows 

[![108](/media/108.png)](/media/108.png)

● To hide the names on pads/traces, select **Settings** from the **Options** menu, select **Misc** and remove the marks on the following items.

[![109](/media/109.png)](/media/109.png)
[![110](/media/110.png)](/media/110.png)

● To export the image, from the **File** menu select **Export**, select **Image**, **Browse** where to export the image with **Top** name.
   ***Make sure to select Monochrome and more than 600 dpi*** then press OK 
 
 [![111](/media/111.png)](/media/111.png)
 [![113](/media/113.png)](/media/113.png)
 
● Shown is the image of the **Top layer** ready for milling. 
  *Traces and pads are shown in White and the background is in Black* 
  [![120](/media/120.png)](/media/120.png)
 
● Hide all visible layers and show only the **Vias**

[![115](/media/115.png)](/media/115.png)

● Export the **Holes** image as **Monochrome** and with **Resolution 700 dpi**

[![116](/media/116.png)](/media/116.png)

● Shown is the image of the **drills**, we will post-process it with GIMP

[![121](/media/121.png)](/media/121.png)

● For the final image, Show only the **Dimension** layer and export it as same as above.

{{< columns >}}
[![117](/media/117.png)](/media/117.png)
<--->
[![118](/media/118.png)](/media/118.png) 
<--->
[![119](/media/119.png)](/media/119.png)
{{< /columns >}}

● At the end of this satge, we have 3 images, one for milling, one for drilling and the third one for cutting the board as shown.

{{< columns >}}
[![Top](/media/Top.png)](/media/Top.png) 
<--->
[![Holes0](/media/Holes0.png)](/media/Holes0.png) 
<--->
[![Outline0](/media/Outline0.png)](/media/Outline0.png)
{{< /columns >}}






















