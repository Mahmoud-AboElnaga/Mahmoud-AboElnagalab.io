---
title: Issues & Solutions
weight: -20
---

## Here I will provide the issues I faced and how I solved them:

**Issue:** When I exported the images from Eagle, the pads and signal names were displayed 

[![106](/media/106.png)](/media/106.png)

**Solution:** Selected **Settings** from the **Options** menu, selected **Misc** and removed the marks on the following items as shown

[![110](/media/110.png)](/media/110.png)

**Issue:** When I opened the **Top.gcode** into **OpenBuilds Controller**, there was an error in the g.code and it did not work for the first time

**Solution:** I opened the GCODE Editor in **OpenBuilds Controller** and removed the Automatic Tool Change line (T1M06), as this machine does not support ATC system.

[![T1M06](/media/T1M06.png)](/media/T1M06.png)

**Issue:** The images I took with my mobile phone were too large (in Mbs) and this take long time to upload and load in the website

**Solution:** I used this [website](https://www.img2go.com/compress-image) to reduce their sizes, also used columns to show images better.
 
[![columns](/media/columns.png)](/media/columns.png) 




