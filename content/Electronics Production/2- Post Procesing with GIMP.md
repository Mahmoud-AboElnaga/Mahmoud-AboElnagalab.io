---
title: 2- Post Processing with GIMP
weight: -20
---
**Now! We are using [GIMP](https://www.gimp.org/) to prepare the images for machining as follows**

   *Traces in White with a Black background*
   
   *Holes in Black with a White background*
   
   *Outline in black with a White background*
   
● The **Top** is ready from the previous stage (Traces in White with a Black background)

[![Top](/media/Top.png)](/media/Top.png)

● To prepare the **drills/holes** image, Open **GIMP**, open the **Holes file**, select **Invert** from **Colors**
  
[![124](/media/124.png)](/media/124.png)

● Now! the 2 drills are in Black with a White background

[![125](/media/125.png)](/media/125.png)

● Export the **drills/holes** as **.png** 

[![126](/media/126.png)](/media/126.png)

● To prepare the **outline** image, open the **Outline file**, select **Invert** from **Colors**
  
[![127](/media/127.png)](/media/127.png)

● Now! the outline is in Black with a White background
● Export the **drills/holes** as **.png** 

[![128](/media/128.png)](/media/128.png)

● At the end of this stage, we have 3 images ready for machining (Top, Drills and Outline)

{{< columns >}}
[![Top](/media/Top.png)](/media/Top.png) 
<--->
[![Holes](/media/Holes.png)](/media/Holes.png) 
<--->
[![Outline](/media/Outline.png)](/media/Outline.png)
{{< /columns >}}


